# ML4T

## Remote access notes

"Your code MUST run properly on the Georgia Tech provided servers, and your code must be submitted to T-square." 

buffet01.cc.gatech.edu if your last name begins with A-G
buffet02.cc.gatech.edu if your last name begins with H-N
buffet03.cc.gatech.edu if your last name begins with O-U
buffet04.cc.gatech.edu if your last name begins with V-Z

```bash
xhost +
ssh -X gtname@buffet0X.cc.gatech.edu
```


