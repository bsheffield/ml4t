"""Assess a betting strategy.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""

"""Instructions

Build a simple gambling simulator

Revise the code in martingale.py to simulate 1000 successive bets on spins of the roulette wheel using the betting scheme outlined above. You should test for the results of the betting events by making successive calls to the get_spin_result(win_prob) function. Note that you'll have to update the win_prob parameter according to the correct probability of winning. You can figure that out by thinking about how roulette works (see wikipedia link above).

Track your winnings by storing them in a numpy array. You might call that array winnings where winnings[0] should be set to 0 (just before the first spin). winnings[1] should reflect the total winnings after the first spin and so on. For a particular episode if you ever hit $80 in winnings, stop betting and just fill the data forward with the value 80.

Experiment 1: Explore the strategy and make some charts

Now we want you to run some experiments to determine how well the betting strategy works. The approach we're going to take is called Monte Carlo simulation where the idea is to run a simulator over and over again with randomized inputs and to assess the results in aggregate. Skip to the "report" section below to which specific properties of the strategy we want you to evaluate.

For the following charts, and for all charts in this class you should use python's matplotlib library. Your submitted project should include all of the code necessary to generate the charts listed in your report. You should configure your code to write the figures to .png files. Do not allow your code to create a window that displays images. If it does you will receive a penalty.

Figure 1: Run your simple simulator 10 times and track the winnings, starting from 0 each time. Plot all 10 runs on one chart using matplotlib functions. The horizontal (X) axis should range from 0 to 300, the vertical (Y) axis should range from -256 to +100. Note that we will not be surprised if some of the plot lines are not visible because they exceed the vertical or horizontal scales.
Figure 2: Run your simple simulator 1000 times. Plot the mean value of winnings for each spin using the same axis bounds as Figure 1. Add an additional line above and below the mean at mean+standard deviation, and mean-standard deviation of the winnings at each point.
Figure 3: Use the same data you used for Figure 2, but plot the median instead of the mean. Be sure to include the standard deviation lines above and below the median as well.
For all of the above charts and experiments, if and when the target $80 winnings is reached, stop betting and allow the $80 value to persist from spin to spin.

Experiment 2: A more realistic gambling simulator

You may have noticed that the strategy actually works pretty well, maybe better than you expected. One reason for this is that we were allowing the gambler to use an unlimited bank roll. In this experiment we're going to make things more realistic by giving the gambler a $256 bank roll. If he or she runs out of money, bzzt, that's it. Repeat the experiments above with this new condition. Note that once the player has lost all of their money (i.e., episode_winnings = -256) stop betting and fill that number (-256) forward. An important corner case to be sure you handle is the situation where the next bet should be $N, but you only have $M (where M<N). Make sure you only bet $M. Here are the two charts to create:

Figure 4: Run your realistic simulator 1000 times. Plot the mean value of winnings for each spin using the same axis bounds as Figure 1. Add an additional line above and below the mean at mean+standard deviation, and mean-standard deviation of the winnings at each point.
Figure 5: Repeat the same experiment as in Figure 4, but use the median instead of the mean. Be sure to include the standard deviation lines above and below the median as well.

"""
  		   	  			    		  		  		    	 		 		   		 		  
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
  		   	  			    		  		  		    	 		 		   		 		  
def author():  		   	  			    		  		  		    	 		 		   		 		  
        return 'bsheffield7' # replace tb34 with your Georgia Tech username.
  		   	  			    		  		  		    	 		 		   		 		  
def gtid():  		   	  			    		  		  		    	 		 		   		 		  
	return 903312988 # replace with your GT ID number
  		   	  			    		  		  		    	 		 		   		 		  
def get_spin_result(win_prob):

	result = False
	if np.random.random() <= win_prob:
		result = True
	return result

def run_realistic_simulator(win_prob, num_loops=1000):

	winnings = list()
	loop_cnt = 0

	episode_winnings = 0
	while episode_winnings < 80 or loop_cnt < num_loops:

		if episode_winnings >= 80:
			winnings.append(80)
			loop_cnt += 1
			continue

		if episode_winnings <= -256:
			winnings.append(-256)
			loop_cnt += 1
			continue

		won = False
		bet_amount = 1
		while not won:
			#wager bet_amount on black
			won = get_spin_result(win_prob) #result of roulette wheel spin

			if won == True:
				episode_winnings = episode_winnings + bet_amount
			else:
				episode_winnings = episode_winnings - bet_amount
				bet_amount = bet_amount * 2
		winnings.append(episode_winnings)
		loop_cnt += 1

	#print(winnings)
	return winnings


def run_simulator(win_prob, num_loops=1000):

	winnings = list()
	loop_cnt = 0

	episode_winnings = 0
	while episode_winnings < 80 or loop_cnt < num_loops:

		if episode_winnings >= 80:
			winnings.append(80)
			loop_cnt += 1
			continue

		won = False
		bet_amount = 1
		while not won:
			#wager bet_amount on black
			won = get_spin_result(win_prob) #result of roulette wheel spin

			if won == True:
				episode_winnings = episode_winnings + bet_amount
			else:
				episode_winnings = episode_winnings - bet_amount
				bet_amount = bet_amount * 2
		winnings.append(episode_winnings)
		loop_cnt += 1

	#print(winnings)
	return winnings

def experiment_one(win_prob):

	#Experiment 1
	tracked_winnings = list()
	for i in range(10):
		winnings = run_simulator(win_prob)
		tracked_winnings.append(winnings)

	#Experiment 1 - Figure 1
	plt.plot(tracked_winnings)
	#plt.show()
	plt.savefig("Figure1.png")


	#Experiment 1 - Figure 2
	tracked_winnings = list()
	tracked_medians = list()
	tracked_stds = list()
	for i in range(1000):
		winnings = run_simulator(win_prob)
		tracked_winnings.append(np.mean(winnings))
		tracked_medians.append(np.median(winnings))
		tracked_stds.append(np.std(winnings))
	plt.plot(tracked_winnings)
	plt.plot(np.add(tracked_winnings,tracked_stds))
	print(tracked_stds)
	plt.plot(np.subtract(tracked_winnings,tracked_stds))
	#plt.show()
	plt.savefig("Figure2.png")

	#Experiment 1 - Figure 3
	plt.plot(tracked_medians)
	plt.plot(np.add(tracked_medians,tracked_stds))
	plt.plot(np.subtract(tracked_medians,tracked_stds))
	plt.savefig("Figure3.png")

def experiment_two(win_prob):

	#Experiment 2 - Figure 4
	tracked_winnings = list()
	tracked_medians = list()
	tracked_stds = list()
	for i in range(1000):
		winnings = run_simulator(win_prob)
		tracked_winnings.append(np.mean(winnings))
		tracked_medians.append(np.median(winnings))
		tracked_stds.append(np.std(winnings))
	plt.plot(tracked_winnings)
	plt.plot(np.add(tracked_winnings,tracked_stds))
	plt.plot(np.subtract(tracked_winnings,tracked_stds))
	plt.savefig("Figure4.png")

	#Experiment 2 - Figure 5
	plt.plot(tracked_medians)
	plt.plot(np.add(tracked_medians,tracked_stds))
	plt.plot(np.subtract(tracked_medians,tracked_stds))
	plt.savefig("Figure5.png")
  		   	  			    		  		  		    	 		 		   		 		  
def test_code():

	#win_prob = 0.60 # set appropriately to the probability of a win
	win_prob = 0.473684 #1 - (10/9) / (10/9 + 1) American Black bets in Roulette
	np.random.seed(gtid()) # do this only once  		   	  			    		  		  		    	 		 		   		 		  
	#print get_spin_result(win_prob) # test the roulette spin
  		   	  			    		  		  		    	 		 		   		 		  
	# add your code here to implement the experiments
	"""
	Roulette Spin Pseudo code


	episode_winnings = $0
	while episode_winnings < $80:
		won = False
		bet_amount = $1
		while not won
			wager bet_amount on black
			won = result of roulette wheel spin
			if won == True:
				episode_winnings = episode_winnings + bet_amount
			else:
				episode_winnings = episode_winnings - bet_amount
				bet_amount = bet_amount * 2
	"""
	plt.xlim(0,100)
	plt.ylim(0,100)
	plt.xlabel("X-axis")
	plt.ylabel("Y-axis")
	experiment_one(win_prob)
	experiment_two(win_prob)

  		   	  			    		  		  		    	 		 		   		 		  
if __name__ == "__main__":  		   	  			    		  		  		    	 		 		   		 		  
    test_code()  		   	  			    		  		  		    	 		 		   		 		  
