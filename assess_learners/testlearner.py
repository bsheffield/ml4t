"""  		   	  			    		  		  		    	 		 		   		 		  
Test a learner.  (c) 2015 Tucker Balch  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
"""

import numpy as np
import math
import sys
import util  # utilizes helper function to read in data
import time
import matplotlib.pyplot as plt
import LinRegLearner as lrl
import DTLearner as dtl
import RTLearner as rtl
import BagLearner as bl
import InsaneLearner as il


def lr_graph():
    # create a learner and train it
    learner = lrl.LinRegLearner(verbose=True)  # create a LinRegLearner
    learner.addEvidence(trainX, trainY)  # train it
    print learner.author()

    # evaluate in sample
    predY = learner.query(trainX)  # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0, 1]

    # evaluate out of sample
    predY = learner.query(testX)  # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0, 1]


def dtl_graph():
    #########DTLearner##########
    # create a learner and train it
    learner = dtl.DTLearner(verbose=False)  # create a Decision Tree Learner
    learner.addEvidence(trainX, trainY)  # train it
    print learner.author()

    # evaluate in sample
    predY = learner.query(trainX)  # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0, 1]

    # evaluate out of sample
    predY = learner.query(testX)  # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0, 1]


def dtl_overfitting():
    # create a learner and train it

    rmses_in_sample = list()
    rmses_out_sample = list()
    leafs = list()
    for i in range(50):
        leafs.append(i+1)
        learner = dtl.DTLearner(verbose=False, leaf_size=i+1)  # create a Decision Tree Learner
        learner.addEvidence(trainX, trainY)  # train it
        predY = learner.query(trainX)  # get the predictions
        in_rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
        rmses_in_sample.append(in_rmse)

        predY = learner.query(testX)  # get the predictions
        out_rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
        print("Leaf size:  " + str(i) + " with a RMSE difference:  " + str(abs(in_rmse - out_rmse)))
        rmses_out_sample.append(out_rmse)

    plt.clf()
    plt.xlabel("Leaf Size")
    plt.ylabel("RMSE")
    plt.legend()
    plt.title("DTLearner Overfitting")
    plt.plot(leafs, rmses_in_sample)
    plt.plot(leafs, rmses_out_sample)
    plt.savefig("DTLearner Overfitting", format="png")
    plt.show()


def dtl_bagging_overfitting():

    rmses_in_sample = list()
    rmses_out_sample = list()
    leafs = list()
    for i in range(50):
        leafs.append(i+1)
        learner = bl.BagLearner(learner=dtl.DTLearner, verbose=False, leaf_size=i+1)  # create a Decision Tree Learner
        learner.addEvidence(trainX, trainY)  # train it
        predY = learner.query(trainX)  # get the predictions
        in_rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
        rmses_in_sample.append(in_rmse)

        predY = learner.query(testX)  # get the predictions
        out_rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
        rmses_out_sample.append(out_rmse)

        print("Leaf size:  " + str(i) + " with a RMSE difference:  " + str(abs(in_rmse - out_rmse)))

    plt.clf()
    plt.xlabel("Leaf Size")
    plt.ylabel("RMSE")
    plt.legend()
    plt.title("DTLearner Bagging Overfitting")
    plt.plot(leafs, rmses_in_sample)
    plt.plot(leafs, rmses_out_sample)
    plt.savefig("DTLearner Bagging Overfitting", format="png")
    plt.show()


def compare_dtl_vs_rtl(is_graph=True):


    dtl_rmses_in_sample = list()
    dtl_rmses_out_sample = list()

    rtl_rmses_in_sample = list()
    rtl_rmses_out_sample = list()

    leafs = [i+1 for i in range(50)]

    dtl_start_time = time.time()
    for i in range(50):
        dtl_learner = dtl.DTLearner(verbose=False, leaf_size=i+1)  # create a Decision Tree Learner
        dtl_learner.addEvidence(trainX, trainY)  # train it
        dtl_predY = dtl_learner.query(trainX)  # get the predictions
        dtl_rmse = math.sqrt(((trainY - dtl_predY) ** 2).sum() / trainY.shape[0])
        dtl_rmses_in_sample.append(dtl_rmse)

        predY = dtl_learner.query(testX)  # get the predictions
        rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
        dtl_rmses_out_sample.append(rmse)
    dtl_end_time = time.time()

    rtl_start_time = time.time()
    for i in range(50):
        rtl_learner = rtl.RTLearner(verbose=False, leaf_size=i+1)
        rtl_learner.addEvidence(trainX, trainY)  # train it
        rtl_predY = rtl_learner.query(trainX)  # get the predictions
        rtl_rmse = math.sqrt(((trainY - rtl_predY) ** 2).sum() / trainY.shape[0])
        rtl_rmses_in_sample.append(rtl_rmse)

        predY = rtl_learner.query(testX)  # get the predictions
        rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
        rtl_rmses_out_sample.append(rmse)

    rtl_end_time = time.time()

    dtl_exec_time = dtl_end_time - dtl_start_time
    rtl_exec_time = rtl_end_time - rtl_start_time

    if is_graph:
        plt.clf()
        plt.xlabel("Leaf Size")
        plt.ylabel("RMSE")
        plt.plot(leafs, dtl_rmses_in_sample, label="DTL In")
        plt.plot(leafs, dtl_rmses_out_sample, label="DTL Out")
        plt.plot(leafs, rtl_rmses_in_sample, label="RTL In")
        plt.plot(leafs, rtl_rmses_out_sample, label="RTL Out")
        plt.legend()
        plt.title("DTLearner vs RTLearner Leafs")
        plt.savefig("DTLearner vs RTLearner Leafs", format="png")
        plt.show()

        plt.clf()
        plt.xlabel("Time")
        plt.ylabel("RMSE")
        dtl_time_chunks = [float(dtl_exec_time / (i + 1)) for i in range(50)]
        rtl_time_chunks = [float(rtl_exec_time / (i + 1)) for i in range(50)]

        plt.plot(dtl_time_chunks, dtl_rmses_in_sample, label="DTL")
        plt.plot(rtl_time_chunks, rtl_rmses_in_sample, label="RTL")
        plt.legend()
        plt.title("DTLearner vs RTLearner Time")
        plt.savefig("DTLearner vs RTLearner Time", format="png")
        plt.show()

    print("Decision Tree Learner finished in time " + str(dtl_exec_time) + " seconds.")
    print("Random Tree Learner finished in time " + str(rtl_exec_time) + " seconds.")

    return dtl_exec_time, rtl_exec_time

def rtl_graph():
    #########RTLearner##########
    # create a learner and train it
    learner = rtl.RTLearner(verbose=False)  # create a Random Tree Learner
    learner.addEvidence(trainX, trainY)  # train it
    print learner.author()

    # evaluate in sample
    predY = learner.query(trainX)  # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0, 1]

    # evaluate out of sample
    predY = learner.query(testX)  # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0, 1]


def bag_graph():
    #######Bag Learner#################
    learner = bl.BagLearner(verbose=False)  # create a Random Tree Learner
    learner.addEvidence(trainX, trainY)  # train it
    print learner.author()

    # evaluate in sample
    predY = learner.query(trainX)  # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0, 1]

    # evaluate out of sample
    predY = learner.query(testX)  # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0, 1]


def insane_graph():
    #######Insane Learner#############
    learner = il.InsaneLearner(verbose=False)  # create a Random Tree Learner
    learner.addEvidence(trainX, trainY)  # train it
    print learner.author()

    # evaluate in sample
    predY = learner.query(trainX)  # get the predictions
    rmse = math.sqrt(((trainY - predY) ** 2).sum() / trainY.shape[0])
    print
    print "In sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=trainY)
    print "corr: ", c[0, 1]

    # evaluate out of sample
    predY = learner.query(testX)  # get the predictions
    rmse = math.sqrt(((testY - predY) ** 2).sum() / testY.shape[0])
    print
    print "Out of sample results"
    print "RMSE: ", rmse
    c = np.corrcoef(predY, y=testY)
    print "corr: ", c[0, 1]


def simulate_dtl_vs_rtl(is_graph=False):

    time_vals = list()
    for i in range(10):
        time_vals.append(compare_dtl_vs_rtl(is_graph))

    print("(DTL,RTL) Execution Times Measured In Seconds")

    for i in range(len(time_vals)):
        print(time_vals[i])


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: python testlearner.py <filename>"
        sys.exit(1)
    data = np.ndarray([])

    with util.get_learner_data_file(sys.argv[1]) as f:
        data = np.genfromtxt(f, delimiter=',')
        # Skip the date column and header row if we're working on Istanbul data
        if sys.argv[1] == 'Istanbul.csv':
            data = data[1:, 1:]

    # compute how much of the data is training and testing  		   	  			    		  		  		    	 		 		   		 		  
    train_rows = int(0.6 * data.shape[0])
    test_rows = data.shape[0] - train_rows

    # separate out training and testing data  		   	  			    		  		  		    	 		 		   		 		  
    trainX = data[:train_rows, 0:-1]
    trainY = data[:train_rows, -1]
    testX = data[train_rows:, 0:-1]
    testY = data[train_rows:, -1]

    print testX.shape
    print testY.shape

    # lr_graph()
    dtl_graph()
    dtl_overfitting()
    dtl_bagging_overfitting()

    compare_dtl_vs_rtl(is_graph=True)

    #run compare_dtl_vs_rtl 10 times with no plotting
    simulate_dtl_vs_rtl(is_graph=False)

    rtl_graph()
    bag_graph()
    insane_graph()
