"""  		   	  			    		  		  		    	 		 		   		 		  
A simple wrapper for linear regression.  (c) 2015 Tucker Balch  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---
"""

import numpy as np
from copy import deepcopy


class RTLearner(object):

    def __init__(self, leaf_size=1, verbose=False, tree=None):
        """
        Initializes Random Tree Learner whether given a set of values.
        :param leaf_size:
        :param verbose:
        :param tree:
        """

        self.leaf_size = leaf_size
        self.verbose = verbose
        self.tree = deepcopy(tree)

    def author(self):
        """
        Returns Georgia Tech username.
        :return:
        """
        return 'bsheffield7'  # replace tb34 with your Georgia Tech username

    def addEvidence(self, dataX, dataY):
        """
        Add data to the tree.
        @summary: Add training data to learner  		   	  			    		  		  		    	 		 		   		 		  
        @param dataX: X values of data to add  		   	  			    		  		  		    	 		 		   		 		  
        @param dataY: the Y training values  		   	  			    		  		  		    	 		 		   		 		  
        """

        #initialize if empty
        if self.tree is None:
            self.tree = self.build(dataX, dataY)
        else:
            self.tree = np.vstack((self.tree, self.build(dataX, dataY)))

        if len(self.tree.shape) == 1:
            self.tree = np.expand_dims(self.tree, axis=0)

    def query(self, points):
        """
        Make a prediction with the decision tree by recursively navigating each row of data.
        @summary: Estimate a set of test points given the model we built.  		   	  			    		  		  		    	 		 		   		 		  
        @param points: should be a numpy array with each row corresponding to a specific query.  		   	  			    		  		  		    	 		 		   		 		  
        @returns the estimated values according to the saved model.  		   	  			    		  		  		    	 		 		   		 		  
        """

        return np.asarray([self.search(p) for p in points])

    def build(self, dataX, dataY):
        """
        Pseudo code:

        build_tree(data)
            if data.shape[0] == 1
                return[leaf, data.y, NA, NA]
            if all data.y same:
                return[leaf,data.y,NA,NA]
            else:
                determine random feature i to split on
                Splitval = (data[random,i] + data[random,i]) / 2
                lefttree = build_tree(data[data:,i] <= SplitVal])
                righttree = build_tree(data[data[:i] > SplitVal])
                root = [i, SplitVal, 1, lefttree.shape[0] + 1]
                return (append(root, lefttree, righttree))

        :param dataX:
        :param dataY:
        :return:
        """

        samples_size = dataX.shape[0]
        features_size = dataX.shape[1]

        leaf = np.array([-1, np.mean(dataY), np.nan, np.nan])

        if len(np.unique(dataY)) == 1 or samples_size <= self.leaf_size:
            return leaf

        features = list(range(features_size))
        random_feature = np.random.choice(features)
        left_index = 0
        right_index = 0
        split_val = 0

        # choose a random feature to split on
        while len(features) > 0:
            random_feature = np.random.choice(features)

            random_samples = [np.random.randint(0, samples_size), np.random.randint(0, samples_size)]

            while random_samples[0] == random_samples[1] and samples_size > 1:
                random_samples = [np.random.randint(0, samples_size), np.random.randint(0, samples_size)]

            split_val = np.mean([dataX[random_samples[0], random_feature],
                                 dataX[random_samples[1], random_feature]])

            left_index = dataX[:, random_feature] <= split_val
            right_index = dataX[:, random_feature] > split_val

            if len(np.unique(left_index)) != 1:
                break

            features.remove(random_feature)

        if len(features) == 0:
            return leaf

        left_tree = self.build(dataX[left_index], dataY[left_index])
        right_tree = self.build(dataX[right_index], dataY[right_index])

        rt_init = 2

        if left_tree.ndim > 1:
            rt_init = left_tree.shape[0] + 1

        root = np.array([random_feature, split_val, 1, rt_init])

        return np.vstack((root, left_tree, right_tree))

    def search(self, point, row=0):
        """
        Helper function for query that recursively traverses the tree for the predicate.
        :param point:
        :param row:
        :return:
        """

        feature, predicate = self.tree[row, 0:2]

        if feature == -1:
            return predicate
        elif point[int(feature)] > predicate:
            return self.search(point, row + int(self.tree[row, 3]))
        else:
            return self.search(point, row + int(self.tree[row, 2]))
