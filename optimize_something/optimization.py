"""MC1-P2: Optimize a portfolio.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		   	  			    		  		  		    	 		 		   		 		  
Atlanta, Georgia 30332  		   	  			    		  		  		    	 		 		   		 		  
All Rights Reserved  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Template code for CS 4646/7646  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Georgia Tech asserts copyright ownership of this template and all derivative  		   	  			    		  		  		    	 		 		   		 		  
works, including solutions to the projects assigned in this course. Students  		   	  			    		  		  		    	 		 		   		 		  
and other users of this template code are advised not to share it with others  		   	  			    		  		  		    	 		 		   		 		  
or to make it available on publicly viewable websites including repositories  		   	  			    		  		  		    	 		 		   		 		  
such as github and gitlab.  This copyright statement should not be removed  		   	  			    		  		  		    	 		 		   		 		  
or edited.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
We do grant permission to share solutions privately with non-students such  		   	  			    		  		  		    	 		 		   		 		  
as potential employers. However, sharing with other current or future  		   	  			    		  		  		    	 		 		   		 		  
students of CS 7646 is prohibited and subject to being investigated as a  		   	  			    		  		  		    	 		 		   		 		  
GT honor code violation.  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
-----do not edit anything above this line---  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988
"""  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
import pandas as pd  		   	  			    		  		  		    	 		 		   		 		  
import matplotlib.pyplot as plt  		   	  			    		  		  		    	 		 		   		 		  
import numpy as np  		   	  			    		  		  		    	 		 		   		 		  
import datetime as dt  		   	  			    		  		  		    	 		 		   		 		  
from util import get_data, plot_data
from scipy.optimize import minimize

def compute_portfolio_stats(allocs, prices, sv=1.0, rfr=0.0, sf=252.0):
    """
    Returns portfolio statistics that are commonly used to assess a portfolio.
    :param allocs: A 1-d Numpy ndarray of allocations to the stocks. All the allocations must be between 0.0 and 1.0 and they must sum to 1.0.
    :param prices:
    :param sv: Share Value of a particular stock
    :param rfr: Rate-Free Return assumed to be zero.
    :param sf: Number of days traded assumed to be 252 days out of the year.
    :return:
    """

    normalized_prices = prices * allocs * sv
    portfolio_value = normalized_prices.sum(axis=1)

    change_in_value = portfolio_value.pct_change()
    cr = (portfolio_value.ix[-1] - sv) / sv
    adr = change_in_value[1:].mean()
    sddr = change_in_value[1:].std()
    sr = sharpe_ratio(prices, allocs)
    return [cr, adr, sddr, sr, portfolio_value]

def sharpe_ratio(prices, allocs, sv=1.0, rfr=0.0, sf=252.0):
    """
    Used to compute the Sharpe Ratio.  This method utilizes the 'traditional shortcut' from Udacity lecture vids.
    :param prices:
    :param allocs: A 1-d Numpy ndarray of allocations to the stocks. All the allocations must be between 0.0 and 1.0 and they must sum to 1.0.
    :param sv: Share Value of a particular stock
    :param rfr: Rate-Free Return assumed to be zero.
    :param sf: Number of days traded assumed to be 252 days out of the year.
    :return:
    """

    normalized_prices = prices * allocs * sv
    portfolio_value = normalized_prices.sum(axis=1)

    change_in_value = portfolio_value.pct_change()
    sr = np.power(1, (1/sf)) * (change_in_value[1:] - rfr).mean() / ((change_in_value[1:]).std())
    return sr

def run_sharpe_ratio(allocs,prices):
    """
    Used to minimize the Sharpe Ratio.
    :param allocs: A 1-d Numpy ndarray of allocations to the stocks. All the allocations must be between 0.0 and 1.0 and they must sum to 1.0.
    :param prices:
    :return:
    """

    return -1.0 * sharpe_ratio(allocs, prices)

# This is the function that will be tested by the autograder  		   	  			    		  		  		    	 		 		   		 		  
# The student must update this code to properly implement the functionality  		   	  			    		  		  		    	 		 		   		 		  
def optimize_portfolio(sd=dt.datetime(2008,1,1), ed=dt.datetime(2009,1,1), \
    syms=['GOOG','AAPL','GLD','XOM'], gen_plot=False):
    """

    :param sd: A datetime object that represents the start date
    :param ed: A datetime object that represents the end date
    :param syms: A list of symbols that make up the portfolio (note that your code should support any symbol in the data directory)
    :param gen_plot: If True, optionally create a plot named plot.png. We will always call your code with gen_plot = False.
    :return: allocs, cr, adr, sddr, sr
    """
  		   	  			    		  		  		    	 		 		   		 		  
    # Read in adjusted closing prices for given symbols, date range  		   	  			    		  		  		    	 		 		   		 		  
    dates = pd.date_range(sd, ed)  		   	  			    		  		  		    	 		 		   		 		  
    prices_all = get_data(syms, dates)  # automatically adds SPY  		   	  			    		  		  		    	 		 		   		 		  
    prices = prices_all[syms]  # only portfolio symbols  		   	  			    		  		  		    	 		 		   		 		  
    prices_SPY = prices_all['SPY']  # only SPY, for comparison later

    prices = prices/prices.ix[0,:] #normalize prices
    num_allocs = len(syms) #number of stocks to allocate for
    allocs = num_allocs*[1.0/num_allocs,] #even amount of allocations to start with

    #fill forwards and backwards with NAN values if any.
    prices.fillna(method="ffill", inplace=True)
    prices.fillna(method="bfill", inplace=True)

    #setup minimizer with the following parameters to minimize the Sharpe Ratio
    constraints = ({'type': 'eq', 'fun': lambda x: 1 - sum(x)})
    bounds = tuple((0, 1) for _ in range(num_allocs))
    minimized_result = minimize(run_sharpe_ratio, allocs, args=(prices,), method="SLSQP", bounds=bounds,
                                constraints=constraints)
    minimized_allocations = minimized_result.x

    #common portfolio statistics measured from portfolio
    cr, adr, sddr, sr, port_val = compute_portfolio_stats(prices, minimized_allocations)

    if gen_plot:
        df_temp = pd.concat([port_val, prices_SPY], keys=['Portfolio', 'SPY'], axis=1)
        df_temp.plot(title="Daily portfolio value and SPY")
        plt.xlabel("Normalized price")
        plt.ylabel("Date")
        plt.savefig('comparison_optimal.png')

    return minimized_allocations, cr, adr, sddr, sr
  		   	  			    		  		  		    	 		 		   		 		  
def test_code():  		   	  			    		  		  		    	 		 		   		 		  
    # This function WILL NOT be called by the auto grader  		   	  			    		  		  		    	 		 		   		 		  
    # Do not assume that any variables defined here are available to your function/code  		   	  			    		  		  		    	 		 		   		 		  
    # It is only here to help you set up and test your code  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    # Define input parameters  		   	  			    		  		  		    	 		 		   		 		  
    # Note that ALL of these values will be set to different values by  		   	  			    		  		  		    	 		 		   		 		  
    # the autograder!  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
    start_date = dt.datetime(2008,6,1)
    end_date = dt.datetime(2009,6,1)
    symbols = ['IBM', 'X', 'GLD', 'JPM']
  		   	  			    		  		  		    	 		 		   		 		  
    # Assess the portfolio  		   	  			    		  		  		    	 		 		   		 		  
    allocations, cr, adr, sddr, sr = optimize_portfolio(sd = start_date, ed = end_date,\
        syms = symbols, \
        gen_plot = True)
  		   	  			    		  		  		    	 		 		   		 		  
    # Print statistics  		   	  			    		  		  		    	 		 		   		 		  
    print "Start Date:", start_date  		   	  			    		  		  		    	 		 		   		 		  
    print "End Date:", end_date  		   	  			    		  		  		    	 		 		   		 		  
    print "Symbols:", symbols  		   	  			    		  		  		    	 		 		   		 		  
    print "Allocations:", allocations  		   	  			    		  		  		    	 		 		   		 		  
    print "Sharpe Ratio:", sr  		   	  			    		  		  		    	 		 		   		 		  
    print "Volatility (stdev of daily returns):", sddr  		   	  			    		  		  		    	 		 		   		 		  
    print "Average Daily Return:", adr  		   	  			    		  		  		    	 		 		   		 		  
    print "Cumulative Return:", cr  		   	  			    		  		  		    	 		 		   		 		  
  		   	  			    		  		  		    	 		 		   		 		  
if __name__ == "__main__":  		   	  			    		  		  		    	 		 		   		 		  
    # This code WILL NOT be called by the auto grader  		   	  			    		  		  		    	 		 		   		 		  
    # Do not assume that it will be called  		   	  			    		  		  		    	 		 		   		 		  
    test_code()  		   	  			    		  		  		    	 		 		   		 		  
